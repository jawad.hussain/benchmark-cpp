# Benchmark cpp

## How to Run:
1. Clone repo using command:
   
   ```- bash
   git clone https://gitlab.com/jawad.hussain/benchmark-cpp.git
   ```
2. Nevigate to root of cloned folder and use the following commands:
   
   ```-bash
    mkdir build
    cd build
   ```
3. For Release mode run the command:
   
   ```-bash
    cmake -DDBUG=OFF ..
    ```
    or
   
    ```-bash 
    cmake ..
   ```
4. For Debug mode run the command:
   
   ```-bash
    cmake -DDBUG=ON ..
    ```
5. Make the file:
    
    ```-bash
    make
    ```
6. Run file in release using command:

    ```-bash
    ./bm_task_release
    ```
    for debug mode:
    
    ```-bash
    ./bm_task_debug
    ```

## Description:
This code demonstrates the use of benchmarking using the google benchmark library.
**NOTE:**   
- Code might take alot of time to run so it is advised to change number of iterations to a smaller value i.e 10000.  
- Also `.gitignore` `contains build_debug` and `build_release`. This is just so both versions can be compiled togather and are not neccessary.