#include <cinttypes>
#include <vector>
#include <list>
#include<time.h>
#include <stdlib.h>
#include <benchmark/benchmark.h>
uint64_t sum_of_vector_squared(int num_entries);
uint64_t sum_of_list_squared(int num_entries);


uint64_t sum_of_vector_squared(int num_entries)
{
    std::vector<int>::iterator it_1;
    std::vector<int> entries;
    int sum;
    srand(time(0));
    for (int it =0;it < num_entries; it++)
    {
        entries.push_back(rand());
    }
    
    std::vector<int> squared_entries;
    for (auto it1: entries)
    {
        squared_entries.push_back(it1*it1);
    }
    for (auto it1: squared_entries)
    {
        sum = sum + it1;
    }

    return sum;

}

uint64_t sum_of_list_squared(int num_entries)
{
    std::list<int>::iterator it_1;
    std::list<int> entries;
    int sum;
    srand(time(0));
    for (int it =0;it < num_entries; it++)
    {
        entries.push_back(rand());
    }
    
    std::list<int> squared_entries;
    for (auto it1: entries)
    {
        squared_entries.push_back(it1*it1);
    }
    for (auto it1: squared_entries)
    {
        sum = sum + it1;
    }

    return sum;
}

static void BM_sum_of_vector_squared(benchmark::State& state)
{
    for(auto _ : state)
    {
        sum_of_vector_squared(state.range(0));
    }
}


static void BM_sum_of_list_squared(benchmark::State& state)
{
    for(auto _ : state)
    {
        sum_of_list_squared(state.range(0));
    }
}

BENCHMARK(BM_sum_of_list_squared)->Arg({100})->Arg({1000})->Arg({10000})->Iterations(1000000);
BENCHMARK(BM_sum_of_vector_squared)->Arg({100})->Arg({1000})->Arg({10000})->Iterations(1000000);

BENCHMARK_MAIN();